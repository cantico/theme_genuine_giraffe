(function($) {
    $.fn.canticoSlider = function() {
        return this.each(function() {
            // Je me stocke quelques données
            var carrousel = $(this);
            var elements = $(this).children();
            var nbElements = elements.length;
            var compteur = 0;

            // Je marque mon territoire
            $(this).addClass('cantico-slider');

            var windowSlider = $('<div></div>').addClass('cantico-slider-window');

            var container = $('<div></div>').addClass('cantico-slider-container').css({
                width : ((nbElements + 2) * 100) + '%'
            });

            if (nbElements > 1) {
                container.css({
                    left : '-100%'
                });
            }

            $(this).wrapInner(container);

            // J'insère mes éléments dans un truc à moi...
            elements.addClass('cantico-slider-element').css({
                width : (100 / (nbElements + 2)) + '%'
            });

            if (nbElements > 1) {
                // Ici, on doit lui redire qui il est, je sais pas encore trop pourquoi...
                container = $(this).children('.cantico-slider-container');

                container.prepend(elements.last().clone());
                container.append(elements.first().clone());
            }

            container.append($('<div></div>').addClass('clear'));
            
            if (nbElements > 1) {       
                var backward = $('<div></div>').addClass('cantico-slider-backward').text('<');
                var forward = $('<div></div>').addClass('cantico-slider-forward').text('>');

                backward.wrapInner($('<div></div>'));
                forward.wrapInner($('<div></div>'));
            }
            
            $(this).wrapInner(windowSlider);
            
            if (nbElements > 1) {
                windowSlider = $(this).find('.cantico-slider-window');

                var timer = $('<div></div>').addClass('cantico-slider-timer').appendTo(windowSlider);

                backward.appendTo($(this));
                forward.appendTo($(this));

                var moving = false;

                var showPrevious = function() {
                    if (!moving) {
                        frame = 0;
                        timer.css({
                            width : 0
                        });
                        moving = true;
                        compteur--;
                        var container = carrousel.find('.cantico-slider-container');

                        container.stop(true, true).animate({
                            left : -((compteur + 1) * 100) + '%'
                        }, function() {
                            if (compteur < 0) {
                                compteur = nbElements - 1;
                                container.css({
                                    left : -((compteur + 1) * 100) + '%'
                                });
                            }
                            moving = false;
                        });
                    }
                };

                var showNext = function() {
                    if (!moving) {
                        frame = 0;
                        timer.css({
                            width : 0
                        });
                        moving = true;
                        compteur++;
                        var container = carrousel.find('.cantico-slider-container');

                        container.stop(true, true).animate({
                            left : -((compteur + 1) * 100) + '%'
                        }, function() {
                            if (compteur >= nbElements) {
                                compteur = 0;
                                container.css({
                                    left : -((compteur + 1) * 100) + '%'
                                });
                            }
                            moving = false;
                        });
                    }
                };

                // On agit au clic sur une flèche
                backward.click(showPrevious);
                forward.click(showNext);

                var frame = 0;

                var hover = false;

                // Si on survole la fenêtre, alors le timer ne doit plus avancer.
                carrousel.hover(function() {
                    hover = true;
                }, function() {
                    hover = false;
                });

                // On réactualise le timer tout le temps
                setInterval(function() {
                    if (!hover) {
                        timer.stop(true, true).animate({
                            width : frame + '%'
                        }, 5 * 1000 / 100, 'linear', function() {
                            if (++frame == 100) {
                                showNext();
                            }
                        });
                    }
                }, 5 * 1000 / 100);
            }
        });
    };
})(jQuery);
