$(document).ready(function() {
    $('.section-title + .section-content:not(.open)').hide();

    $('.section-title').css({
        'cursor' : 'pointer'
    }).click(function() {
      $(this).next('.section-content').stop().slideToggle();
    });
});
