jQuery(document).ready(function() {
    jQuery('a[href^="#"]').click(function(){
        var the_id = $(this).attr("href").substr(1);
        if (the_id != '') {
            var pos = $('a[name='+the_id+']').offset().top - 100;
            if (pos < 0)
                pos = 0;
            $(window).scrollTop(pos);
            return false;
        }
    });
});
