jQuery(document).ready(function() {
    var lien = jQuery('.return-top');
    var hidden = false;

    if (jQuery(window).scrollTop() == 0) {
        lien.hide();
        var hidden = true;
    }

    jQuery(window).scroll(function() {
        if (jQuery(window).scrollTop() == 0 && !hidden) {
            lien.stop().fadeOut();
            hidden = true;
        }
        else if (jQuery(window).scrollTop() != 0 && hidden) {
            lien.stop().hide().fadeIn();
            hidden = false;
        }
    });

    var scrollTo = function(obj, pos) {
        var scrollTop = obj.scrollTop();
        if (Math.abs(scrollTop - pos) <= 10)
            obj.scrollTop(pos);
        else {
            obj.scrollTop(scrollTop - (scrollTop - pos) / 3);
            setTimeout(function() {
                scrollTo(obj, pos);
            }, 25);
        }
    };

    lien.click(function() {
        scrollTo(jQuery(window), 0);

        return false;
    });
});
