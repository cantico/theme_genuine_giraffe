<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';

/**
 * @return bab_Path
 */
function theme_genuine_giraffe_getLogoImagePath()
{
    $addon = bab_getAddonInfosInstance('theme_genuine_giraffe');

    $ovidentiapath = realpath('.');

    $uploadPath = new bab_Path($ovidentiapath, 'images',  $addon->getRelativePath(), 'logo');
    if ($uploadPath->isDir()) {
        $uploadPath->createDir();
    }

    return $uploadPath;
}

/**
 * Instanciates the widget factory.
 *
 * @return Func_Widgets
 */
function theme_genuine_giraffe_Widgets()
{
    return bab_Functionality::get('Widgets');
}

/**
 * @param string $str
 *
 * @return string
 */
function theme_genuine_giraffe_translate($str)
{
    return bab_translate($str, 'theme_genuine_giraffe');
}

/**
 *
 * @param string        $text
 * @param Widget_Widget $widget
 */
function theme_genuine_giraffe_LabelledWidget($text, $widget)
{
    $W = theme_genuine_giraffe_Widgets();
    $label = $W->Label(theme_genuine_giraffe_translate($text));
    $label->setAssociatedWidget($widget);
    $label->colon();

    return $W->FlowItems($label, $widget)->setHorizontalSpacing(1, 'em');
}

/**
 * Displays a form to edit the theme configuration.
 */
function theme_genuine_giraffe_editConfiguration()
{
    bab_functionality::get('Icons');

    $W = theme_genuine_giraffe_Widgets();
    $page = $W->BabPage();

    $page->setLayout($W->VBoxLayout());

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/theme_genuine_giraffe/global');

    $form = $W->Form();

    $form->addClass(Func_Icons::ICON_LEFT_16);

    $headerSection = $W->Section(
        theme_genuine_giraffe_translate('Header'),
        $W->VBoxItems(
            theme_genuine_giraffe_LabelledWidget(theme_genuine_giraffe_translate('Logo'),
                $logoImagePicker = $W->ImagePicker()
                    ->oneFileMode()
                    ->setDimensions(200, 200)
                    ->setName('logoImage')
            ),
            theme_genuine_giraffe_LabelledWidget(theme_genuine_giraffe_translate("Display site name"),
                $W->Checkbox()
                    ->setName('showsitename')
                    ->setValue($registry->getValue('showsitename'))
            ),
            theme_genuine_giraffe_LabelledWidget(theme_genuine_giraffe_translate("Background image"),
                $W->Select()
                    ->addOption('', theme_genuine_giraffe_translate('None'))
                    ->addOption('giraffe.png', theme_genuine_giraffe_translate('Giraffe (default)'))
                    ->addOption('trame.png', 'Trame')
                    ->setName('headerimage')
                    ->setValue($registry->getValue('headerimage'))
            )
        )
    );

    $footerSection = $W->Section(
        theme_genuine_giraffe_translate('Footer'),
        $W->VBoxItems(
            theme_genuine_giraffe_LabelledWidget(theme_genuine_giraffe_translate("Background image"),
                $W->Select()
                    ->addOption('', theme_genuine_giraffe_translate('None'))
                    ->addOption('trame.png', theme_genuine_giraffe_translate('Trame (default)'))
                    ->setName('footerimage')
                    ->setValue($registry->getValue('footerimage'))
            )
        )
    );

    $nodeSection = $W->Section(
        theme_genuine_giraffe_translate('Navigation nodes'),
        $W->VBoxItems(
            theme_genuine_giraffe_LabelledWidget(theme_genuine_giraffe_translate('Top navigation node'),
                $W->SitemapItemPicker()
                    ->setName('top_navigation_node')
                    ->setValue($registry->getValue('top_navigation_node'))
            ),
            theme_genuine_giraffe_LabelledWidget(theme_genuine_giraffe_translate('Bottom navigation node'),
                $W->SitemapItemPicker()
                    ->setName('bottom_navigation_node')
                    ->setValue($registry->getValue('bottom_navigation_node'))
            )
        )
    );

    $colorSection = $W->Section(
        theme_genuine_giraffe_translate('Colors'),
        $W->VBoxItems(
            theme_genuine_giraffe_LabelledWidget(
                theme_genuine_giraffe_translate('Header background'),
                $W->ColorPicker()
                    ->setName('headerBackgroundColor')
                    ->setValue(substr($registry->getValue('headerBackgroundColor'), 1))
            ),
            theme_genuine_giraffe_LabelledWidget(
                theme_genuine_giraffe_translate('Footer background'),
                $W->ColorPicker()
                    ->setName('footerBackgroundColor')
                    ->setValue(substr($registry->getValue('footerBackgroundColor'), 1))
            ),
            theme_genuine_giraffe_LabelledWidget(
                theme_genuine_giraffe_translate('Title color'),
                $W->ColorPicker()
                    ->setName('titleColor')
                    ->setValue(substr($registry->getValue('titleColor'), 1))
            ),
            theme_genuine_giraffe_LabelledWidget(
                theme_genuine_giraffe_translate('Link color'),
                $W->ColorPicker()
                    ->setName('linkColor')
                    ->setValue(substr($registry->getValue('linkColor'), 1))
            )->setVerticalSpacing(30, 'px')
        )
    );

    $form->addItem(
        $W->Frame()->setLayout(
            $W->VBoxItems(
                $headerSection,
                $footerSection,
                $nodeSection,
                $colorSection
            )->setVerticalSpacing(20, 'px')
        )->setName('configuration')
    );

    $form->addItem(
        $W->FlowItems(
            $W->SubmitButton()
                ->setName('idx[save]')
                ->setLabel(theme_genuine_giraffe_translate('Save configuration')),
            $W->SubmitButton()
                ->setName('idx[reset]')
                ->setLabel(theme_genuine_giraffe_translate('Reset configuration')),
            $W->SubmitButton()
                ->setName('idx[cancel]')
                ->setLabel(theme_genuine_giraffe_translate('Cancel'))
        )->setHorizontalSpacing(1, 'em')
    );

    //$logoImagePicker->setFolder(theme_genuine_giraffe_getLogoImagePath());

    $logoImagePickerFolder = $logoImagePicker->getFolder();

    try {
        $logoImagePickerFolder->deleteDir();
    } catch (bab_FolderAccessRightsException $e) {}

    $logoImagePickerFolder->createDir();
    $logoImageFolder = theme_genuine_giraffe_getLogoImagePath('logo');

    foreach ($logoImageFolder as $logoImageFile) {
        $basename = $logoImageFile->getBasename();
        copy($logoImageFile->toString(), $logoImagePickerFolder->toString() . '/' . $basename);
    }

    $form->setHiddenValue('tg', bab_rp('tg'));

    $page->setTitle(theme_genuine_giraffe_translate('Theme configuration'));
    $page->addItem($form);

    $page->displayHtml();
}

/**
 * Saves the posted configuration.
 *
 * @param array $configuration
 */
function theme_genuine_giraffe_saveConfiguration($configuration, $reset=false)
{
    $W = bab_Widgets();

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/theme_genuine_giraffe/global');

    $addon = bab_getAddonInfosInstance('theme_genuine_giraffe');

    if (isset($configuration['top_navigation_node']) && is_string($configuration['top_navigation_node'])) {
        $registry->setKeyValue('top_navigation_node', $configuration['top_navigation_node']);
    }

    if (isset($configuration['bottom_navigation_node']) && is_string($configuration['bottom_navigation_node'])) {
        $registry->setKeyValue('bottom_navigation_node', $configuration['bottom_navigation_node']);
    }

    $logoImagesPath = theme_genuine_giraffe_getLogoImagePath('logo');

    try {
        $logoImagesPath->deleteDir();
    } catch (bab_FolderAccessRightsException $e) {}

    $logoImagesPath->createDir();

    $logoFiles = $W->ImagePicker()->getTemporaryFiles('logoImage');

    $logoImage = "";

    if (isset($logoFiles)) {
        foreach ($logoFiles as $logoFile) {
            $original = $logoFile->getFilePath()->toString();
            rename($original, $logoImagesPath->toString() . '/' . basename($original));
            $logoImage = 'images/' . $addon->getRelativePath() . 'logo/' . basename($original);
        }
    }

    if (isset($configuration['logoImage']) && ($configuration['logoImage'] === '')) {
        $logoImagesPath->deleteDir();
        $registry->setKeyValue('logoImage', '');
    }
    else {
        $registry->setKeyValue('logoImage', $logoImage);
    }

    $registry->setKeyValue('showsitename', $configuration['showsitename']);
    $registry->setKeyValue('headerimage', $configuration['headerimage']);
    $registry->setKeyValue('footerimage', $configuration['footerimage']);
    $registry->setKeyValue('headerBackgroundColor', '#' . $configuration['headerBackgroundColor']);
    $registry->setKeyValue('footerBackgroundColor', '#' . $configuration['footerBackgroundColor']);
    $registry->setKeyValue('titleColor', '#' . $configuration['titleColor']);
    $registry->setKeyValue('linkColor', '#' . $configuration['linkColor']);

    /* @var $Less Func_Less */
    $Less = bab_functionality::get('less');

    try {
        $Less->removeCompiledFiles();

        if($reset === true)
            $GLOBALS['babBody']->addMessage(theme_genuine_giraffe_translate('Default configuration restored'));
        else
            $GLOBALS['babBody']->addMessage(theme_genuine_giraffe_translate('Configuration saved'));
    } catch(bab_FileAccessRightsException $e) {
        bab_debug($e->getMessage());
    }

    theme_genuine_giraffe_editConfiguration();
}

/**
 * Resets configuration
 */
function theme_genuine_giraffe_resetConfiguration() {
    $configuration = array(
        'headerimage'            => 'giraffe.png',
        'footerimage'            => 'trame.png',
        'showsitename'           => '1',
        'logoImage'              => '',
        'top_navigation_node'    => '',
        'bottom_navigation_node' => '',
        'headerBackgroundColor'  => '232F4D',
        'footerBackgroundColor'  => '232F4D',
        'titleColor'             => '232F4D',
        'linkColor'              => 'FFFFFF'
    );

    $uploadPath = theme_genuine_giraffe_getLogoImagePath();

    try {
        $uploadPath->deleteDir();
    }
    catch (Exception $e){}

    theme_genuine_giraffe_saveConfiguration($configuration, true);
}

// Exécution

if (!bab_isUserAdministrator()) {
    $babBody->addError(theme_genuine_giraffe_translate('Access denied.'));
    return;
}

$idx = bab_rp('idx', 'edit');

if (is_array($idx)) {
    list($idx,) = each($idx);
}

switch ($idx) {
    case 'save':
        $addon = bab_getAddonInfosInstance('theme_genuine_giraffe');
        $configuration = bab_rp('configuration', array());
        theme_genuine_giraffe_saveConfiguration($configuration);
        break;
    case 'reset':
        theme_genuine_giraffe_resetConfiguration();
        break;
    case 'cancel':
        $addon = bab_getAddonInfosInstance('theme_genuine_giraffe');
        theme_genuine_giraffe_redirect('?tg=addons&idx=theme');
        break;
    case 'edit':
    default:
        theme_genuine_giraffe_editConfiguration();
        break;
}
