<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

function theme_genuine_giraffe_upgrade($version_base, $version_ini)
{
    global $babDB;
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    bab_addEventListener('bab_eventPageRefreshed', 'theme_genuine_giraffe_onPageRefreshed', 'addons/theme_genuine_giraffe/init.php', 'theme_genuine_giraffe');

    $func = new bab_functionalities;
    $func->register('Ovml/Function/ThemeGenuineGiraffeGetConfigurationValue', dirname(__FILE__).'/ovml_configuration.php');

    return true;
}

// Lors de la suppression du module
function theme_genuine_giraffe_onDeleteAddon() {
    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    bab_removeEventListener('bab_eventPageRefreshed', 'theme_genuine_giraffe_onPageRefreshed', 'addons/theme_genuine_giraffe/init.php');
    
    $func = new bab_functionalities;
    $func->unregister('Ovml/Function/ThemeGenuineGiraffeGetConfigurationValue');

    return true;
}

function theme_genuine_giraffe_onPageRefreshed() {
    global $babBody;

    if ('theme_genuine_giraffe' === $GLOBALS['babSkin']) {
        require_once dirname(__FILE__) . '/functions.php';

        $icons = bab_functionality::get('Icons');
        $icons->includeCss();

        /* @var $Less Func_Less */
        if ($Less = @bab_functionality::get('less')) {
            $variables = theme_genuine_giraffe_getLessVariables();

            $path = realpath('.') . '/skins/theme_genuine_giraffe/styles/';

            /* Workaround to correctly display base and extension for header and footer backgrounds */
            if (!($variables["headerimage"] === "none")) {
                $extension = explode(".", $variables["headerimage"]);

                if (isset($extension[1]))
                    $variables["extensionHeader"] = $extension[1];
            }

            if (!($variables["footerimage"] === "none")) {
                $extension = explode(".", $variables["footerimage"]);

                if (isset($extension[1]))
                    $variables["extensionFooter"] = $extension[1];
            }

            try {
                $stylesheet = $Less->getCssUrl($path.'style.less', $variables);
                $babBody->addStyleSheet($stylesheet);
            } catch (Exception $e) {
                bab_debug($e->getMessage());
            }
        }
    }
}
