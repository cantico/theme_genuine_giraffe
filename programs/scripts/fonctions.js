function modifTailleTexte(parPlusOuMoins){
	var pas;
	var tailleMax;
	var tailleMin;
	var unite;
	var tailleBrut = jQuery('body').css('font-size');
	var tailleActuelle = parseFloat(tailleBrut);
	var nouvelleTaille;
	if(tailleBrut.indexOf("%") != -1)
		unite = "%";
	else
		unite = tailleBrut.substr(tailleBrut.length-2);
	switch(unite){
		case "em":
			tailleMax = 1.2;
			tailleMin = 0.75;
			pas = 0.1;
			break;
		case "px":
			tailleMax = 18.5;
			tailleMin = 12.5;
			pas = 2;
			break;
		case "pt":
			tailleMax = 14;
			tailleMin = 9;
			pas = 1;
			break;
		case "%":
			tailleMax = 120;
			tailleMin = 75;
			pas = 10;
			break;
		default:
			alert("Impossible de redimensionner le texte (l'unit� utilis�e pour fixer la taille de la police n'est pas prise en charge)");
			return false;
			break;
	}
	
	if(tailleActuelle != "NaN"){
		if(parPlusOuMoins == "moins" && tailleActuelle > tailleMin){
			nouvelleTaille = tailleActuelle - pas;
			var nouvelleTailleBloc = jQuery('#categHaut').height() - 70;
		}else if(parPlusOuMoins == "plus" && tailleActuelle < tailleMax){
			nouvelleTaille = tailleActuelle + pas;
			//alert("Taille brut:"+tailleBrut+" - Taille actuelle:"+tailleActuelle+" - Pas:"+pas+" - Nouvelle taille:"+nouvelleTaille);
			var nouvelleTailleBloc = jQuery('#categHaut').height() + 70;
		}else 
			return false;
		jQuery('body').css('font-size',nouvelleTaille+unite);
		if(jQuery('#categHaut').length >0)
			jQuery('#categHaut').height(nouvelleTailleBloc + "px");
		//On sauvegarde la taille dans un cookie pour la conserver lors de la navigation
		setCookie("taillePolice",nouvelleTaille+unite,'1');
	}
}

function setCookie(c_name,value,exdays){
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name){
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++){
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name)
			return unescape(y);
	}
}
