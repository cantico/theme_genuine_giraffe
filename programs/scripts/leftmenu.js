var zInterval = null;
var SLIDE_STEP = 8;


function doSlide(dX) {
	var slideObj = document.getElementById('leftcontent');
	x = slideObj.offsetLeft;
	if(x+SLIDE_STEP<dX) {
		// div is less than its destination, move it to the right
		x+=SLIDE_STEP;
		slideObj.style.left = x + "px";
	} else if (x-SLIDE_STEP>dX) {
		// div is more than its destination, move to the left
		x-=SLIDE_STEP;
		slideObj.style.left = x + "px";
	} else  {
		// div is within the boundaries of its destination. put it where its supposed to be
		// and clear the interval
		slideObj.style.left = dX + "px";
		clearInterval(zInterval);
		zInterval = null;
	}
}


function handlePanelClick()
{
	clearInterval(zInterval);

	var leftpanel = document.getElementById('leftcontent');
	if (leftpanel.style.left == '0px')
		{
		intervalMethod = function() { doSlide(-161); }
		}
	else
		{
		intervalMethod = function() { doSlide(0); }
		}
	zInterval = setInterval(intervalMethod,10);
}

function setLeftPanel() {
	if (document.getElementById('leftcontent'))
		{
		var leftpanel = document.getElementById('leftcontent');
		leftpanel.style.position = 'absolute';
		leftpanel.style.zIndex = 50;
		leftpanel.style.display = 'block';
		leftpanel.style.left = '-161px';
		leftpanel.style.top = '20px';
		leftpanel.onclick = handlePanelClick;
		var left_menu_btn = document.createElement("a");
		left_menu_btn.id = "left_menu_btn";
		leftpanel.appendChild(left_menu_btn);
	}
}

window.onload = function() {  setLeftPanel(); };
