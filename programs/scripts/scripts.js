(function($) {
    $(document).ready(function() {
        // Survol des liens d'accès rapides
        $('#pictos a').hover(function() {
            $('#pictos a').not($(this)).stop().animate({ 'opacity': 0.5 }, 100, 'linear');
        }, function() {
            $('#pictos a').stop().animate({ 'opacity': 1 }, 100, 'linear');
        });
    });
    
    $(document).ready(function() {
        // Affichage des sous-menus du menu principal
	 $('.panell').hide();

	/* On cherche si un lien est actif */
	var activeLink = $("li.menu > a.active");
	
	/* Si trouv�, on d�plie le panel correspondant || ABANDONN�
	if(activeLink){
		var idPan = 'panell-' + activeLink.attr('data-panell');
		var panelToOpen = $('#'+idPan);
		panelToOpen.slideToggle(400);
	}
	*/
   $('.menu a').mouseover(function(e) {
   	var id = 'panell-' + $(this).attr('data-panell');
   	var hasOpenedPanel = false;
		var panell = $('#'+id);
            
				            
   	$('.panell').each(function(i) {
      	if( $(this).attr('id') != id) {
      	$(this).stop(true, true).slideUp(50);
         	hasOpenedPanel = true;
      	}
   	});
        
            	
      if(panell.length > 0) {
         panell.stop(true, true).slideToggle();
         $(this).toggleClass('active');
         $('.menu a').not(this).each(function(){
            $(this).removeClass('active');
         });
			e.preventDefault();
                
         return false;
      }
      /*else {
         panell.stop().show(true, true).slideToggle();
         $(this).toggleClass('active');
      }*/
            
      	return true;
		});
  
	$('#ei_menu').mouseover(function() {
			$('.panell').each(function(){ $(this).stop(true, true).slideUp(50); });
	 });
	});
	 
	 

})(jQuery);
