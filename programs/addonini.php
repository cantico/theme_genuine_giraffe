; <?php/*
 
[general]
name                            ="theme_genuine_giraffe"
version                         ="1.0"
description                     ="Fully customizable skin through the Less library"
description.fr                  ="Skin entièrement personnalisable grâce à la librairie Less"
delete                          ="1"
addon_access_control            ="0"
ov_version                      ="7.8.9"
addon_type                      ="THEME"
author                          ="Cantico"
encoding                        ="UTF-8"
mysql_character_set_database    ="latin1,utf8"
php_version                     ="5.1.0"
configuration_page              ="configuration"
icon                            ="icon.png"
image                           ="thumbnail.png"

[addons]
portlets            ="0.4"
sitemap_editor      ="0.5.6"
LibLess             ="0.3.8.0"

;*/ ?>
